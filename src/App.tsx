import React from 'react';
import './App.css';
import PageLinks from './pages/PageLinks/PageLinks';

function App() {

  return (
    <div className="App">
      I told you not to click this, but since you're here, check out this version I'm making myself
      <PageLinks />
    </div>
  );
}

export default App;
