import * as React from 'react';
import './PageLinks.css';
import { Button, ButtonGroup } from '@material-ui/core';
import { PropTypes } from '@material-ui/core/index';
import { detect } from 'detect-browser';

export interface Props {
}

export interface State {
}

export default class PageLinks extends React.PureComponent<Props, State> {

    private links = [
        {
            title: 'IG: c0lorblindkid',
            url: 'https://www.instagram.com/c0lorblindkid/',
            adult: false,
            color: 'primary' as PropTypes.Color,
        },
        {
            title: 'TikTok: c0lorblindkid',
            url: 'https://www.tiktok.com/@c0lorblindkid',
            adult: false,
            color: 'primary' as PropTypes.Color,
        },
        {
            title: 'FetLife: flyforahighguy',
            url: 'https://fetlife.com/users/11362444',
            adult: true,
            color: 'secondary' as PropTypes.Color,
        },
        {
            title: 'OnlyFans: flyforahighguy',
            url: 'https://onlyfans.com/flyforahighguy',
            adult: true,
            color: 'secondary' as PropTypes.Color,
        },
    ];

    render() {
        const linkDisplay = [];
        if (this.links.length > 0) {
            linkDisplay.push(<h2 key={Math.random()} style={{ textAlign: 'center' }}>Links</h2>);
            const linkButtons = [];

            for (const link of this.links) {
                linkButtons.push(
                    <Button
                        key={Math.random()}
                        color={link.color}
                        style={{ margin: '10px' }}
                        size={'large'}
                        href={link.url}
                    >
                        {link.title}
                    </Button>
                );
            }

            linkDisplay.push(
                <ButtonGroup
                    key={Math.random()}
                    variant={'outlined'}
                    orientation={'vertical'}
                >
                    {linkButtons}
                </ButtonGroup>
            )
        }

        return (
            <div className='AboutPage'>
                <div className='AboutText'>
                    {linkDisplay}
                    <pre>{JSON.stringify({ ...detect(), randomNumber: Math.random() * 100 }, null, 2)}</pre>
                </div>
            </div>
        );
    }
}